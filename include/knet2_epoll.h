#ifndef KNET2_EPOLL_H
#define KNET2_EPOLL_H
#include <stdlib.h>
#include <stddef.h>

#include <knet2.h>

typedef struct knet2_epoll_handler_t {
	knet2_ip_handler_t ip_base;

	int epoll_fd;

	size_t max_events;
	struct epoll_event *events;

} knet2_epoll_handler_t;


int knet2_epoll_wait(knet2_ip_handler_t *vnet, knet2_event_t *events, size_t max_events, knet2_error_t *error);
int knet2_epoll_connect(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr, knet2_error_t *error);
int knet2_epoll_listen(knet2_ip_handler_t *vnet, knet2_error_t *error);
int knet2_epoll_accept(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli, knet2_error_t *error);
int knet2_epoll_handler_init_allocs(knet2_epoll_handler_t *unintialized_net, int max_events, knet2_error_t *error);
void knet2_epoll_handler_release_frees(knet2_ip_handler_t *vnet);
void knet2_epoll_release_addr(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr);

int knet2_epoll_add_watched_addr(knet2_ip_handler_t *net, knet2_ip_addr_t *addr);
int knet2_epoll_mod_watched_addr(knet2_ip_handler_t *net, knet2_ip_addr_t *addr, uint32_t flags);
int knet2_epoll_del_watched_addr(knet2_ip_handler_t *net, knet2_ip_addr_t *addr);
#endif
