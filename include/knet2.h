/**
 * @file knet2.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Easy ip funcs for net
 *
 * base ip implementation of knet2 @@.
 * @see https://klib.gitlab.io/libknet/
 */
#ifndef KNET2
#define KNET2
#include <stddef.h>
#include <netinet/in.h>

#include <knet2.h>

typedef struct knet2_ip_handler_t knet2_ip_handler_t;

#define KNET2_EV_IN  0x00000001
#define KNET2_EV_OUT 0x00000002
#define KNET2_EV_ERR 0x80000000
typedef struct knet2_event_t {
	int fd;
	uint32_t flags;
} knet2_event_t;


#define KNET2_ERROR_MSG_LEN 64
typedef struct knet2_error_t {
	int errcode;
	char knet2_errmsg[KNET2_ERROR_MSG_LEN];
	char proto_errmsg[KNET2_ERROR_MSG_LEN];
} knet2_error_t;

void knet2_set_knet2_error(knet2_error_t *error, int errcode, char *proto_error, const char *format, ...);

int knet2_interrupt(knet2_ip_handler_t *net, knet2_error_t *error);


/**
 * @brief The necessary bits for tcp communication
 *
 */
typedef struct knet2_ip_addr_t {
	struct sockaddr_in saddr;
	int sockfd;
} knet2_ip_addr_t;



/**
 * @brief ip should be host, but the presentation of tcp_addr_t
 *
 */
typedef struct knet2_ip_host_port_t {
	char host[256];
	char port[6];
} knet2_ip_host_port_t;


/**
 * @brief Each implementation will assign it's corresponding function ptrs here.
 *
 */
typedef struct knet2_ip_handler_t {
	uint32_t select_timeout_usec;

	int interrupt_fd;

	uint16_t listen_backlog;

	int (*addr_resolve_host)(const char *host, knet2_ip_addr_t* dest, knet2_error_t *error);
	int (*addr_parse)(knet2_ip_addr_t *addr, const char *host, knet2_error_t *err);
	int (*addr_reverse)(knet2_ip_addr_t *addr, char *buffer, size_t buflen, knet2_error_t *err);
	void (*release_addr)(knet2_ip_handler_t *net, knet2_ip_addr_t *addr);
	int (*accept)(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli, knet2_error_t *err);
	int (*listen)(knet2_ip_handler_t *vnet, knet2_error_t *err);
	int (*connect)(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli, knet2_error_t *err);
	size_t (*recv)(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli, uint8_t *buffer, size_t buffer_size, knet2_error_t *err);
	size_t (*send)(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli, uint8_t *buffer, size_t buffer_size, knet2_error_t *err);
	int (*wait)(knet2_ip_handler_t *vnet, knet2_event_t *events, size_t max_events, knet2_error_t *err);
	int (*add_watched_addr)(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr);
	int (*mod_watched_addr)(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr, uint32_t flags);
	int (*del_watched_addr)(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr);
	void (*release_frees)(knet2_ip_handler_t *vnet);
	size_t sizeof_addr;
	knet2_ip_addr_t this_addr;

} knet2_ip_handler_t;


/**
 * @brief Closes the connection
 *
 * @param vnet p_vnet:...
 * @param addr p_addr:...
 */
void knet2_ip_release_addr(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr);

/**
 * @brief Given hostname only, resolves to network
 *
 * @param vaddr p_vaddr:...
 * @param str_addr p_str_addr:...
 * @return int
 */
int knet2_ip_addr_resolve_host(const char *host, knet2_ip_addr_t* dest, knet2_error_t *error);

/**
 * @brief Given str_addr, parses into vaddr
 *
 * @param vaddr p_vaddr:...
 * @param str_addr p_str_addr:...
 * @return int
 */
int knet2_ip_addr_parse(knet2_ip_addr_t *vaddr, const char* str_addr, knet2_error_t *error);

/**
 * @brief Given vaddr, provides the IP and port in ip:port form.
 *
 * @param vaddr p_vaddr:...
 * @param buffer p_buffer:...
 * @param buflen p_buflen:...
 * @return int
 */
int knet2_ip_address_reverse(knet2_ip_addr_t *vaddr, char *buffer, size_t buflen, knet2_error_t *error);

/**
 * @brief Splits str_addr into host and port in host_ip
 *
 * @param host_ip p_host_ip:...
 * @param str_addr p_str_addr:...
 * @return int
 */
int knet2_ip_parse_host_port(knet2_ip_host_port_t *host_ip, const char *str_addr);

/**
 * @brief Initializes the tcp handler
 *
 * @param tcp p_tcp:...
 * @param error p_error:...
 * @return int
 */
int knet2_ip_init(knet2_ip_handler_t *net, knet2_error_t *error);

/**
 * @brief Internally used
 *
 * @param sockfd p_sockfd:...
 * @return int
 */
int knet2_ip_set_socket_options(int sockfd, knet2_error_t *error);


/**
 * @brief Nonblocking accept
 *
 * @param vnet p_vnet:...
 * @param vcli p_vcli:...
 * @return int
 */
int knet2_ip_accept(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli, knet2_error_t *error);

/**
 * @brief Sets up a server
 *
 * @param vnet p_vnet:...
 * @return int
 */
int knet2_ip_listen(knet2_ip_handler_t *vnet, knet2_error_t *error);

/**
 * @brief Creates client connection
 *
 * @param vnet p_vnet:...
 * @param addr p_addr:...
 * @return int
 */
int knet2_ip_connect(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr, knet2_error_t *error);

/**
 * @brief Will attempt to receive
 *
 * @param vnet p_vnet:...
 * @param vaddr p_vaddr:...
 * @param buffer p_buffer:...
 * @param buffer_size p_buffer_size:...
 * @return size_t
 */
size_t knet2_ip_recv(knet2_ip_handler_t* vnet, knet2_ip_addr_t* vaddr, uint8_t *buffer, size_t buffer_size, knet2_error_t *error);

/**
 * @brief Attempts to send data
 *
 * @param vnet p_vnet:...
 * @param vaddr p_vaddr:...
 * @param buffer p_buffer:...
 * @param buffer_size p_buffer_size:...
 * @return size_t
 */
size_t knet2_ip_send(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vaddr, uint8_t *buffer, size_t buffer_size, knet2_error_t *error);

#endif
