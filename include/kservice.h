/**
 * @file kservice.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Easy funcs for network service and client
 *
 * Networking wasn't easy to use without this @@.
 * @see https://klib.gitlab.io/libknet/
 */
#ifndef KSERVICE_H
#define KSERVICE_H

#include <stdint.h>
#include <khashmap.h>
#include <knet2.h>


#define KSERVICE_LOCK(mutex)
#define KSERVICE_UNLOCK(mutex)
#define KSERVICE_MAX_ADDR_LENGTH 256


typedef struct kservice_write_buffer_t {
	size_t size;
	size_t index;           //index to start writing at in data.
	uint8_t data[0];        //the size of write buffer will extend beyond here by size.
} kservice_write_buffer_t;


typedef struct kservice_t kservice_t;
/**
 * @brief A network service structure
 *
 */
typedef struct kservice_client_t {
	uint64_t last_seen_time;
	knet2_ip_addr_t *addr;
	klist_t write_buffers;
} kservice_client_t;


/**
 * @brief Used
 *
 */
/**
 * @brief Callback with information about a new client accepted
 *
 */
typedef int (*kservice_handle_client_f) (kservice_t *service, kservice_client_t *client, uint8_t *buffer, size_t buflen);

/**
 * @brief Callback from within the network event loop
 *
 */
typedef void (*kservice_handle_loop_iteration_f)(kservice_t *service, void *context);

/*
 * TODO: This replaces handle_client - it's up to the service to handle the flags given.
 * If it is the interrupt, if it is the accept, if it is a client read, or a client writable.
 */
typedef void (*kservice_handle_io_event_f) (kservice_t *service, kservice_client_t *client, knet2_event_t *fd_event);

/**
 * @brief Simple structure for a service.
 * Extra data is added by using an overlay struct with this type as first field.
 *
 * struct myservice_t {
 *   kservice_t base;// A myservice_t B,  will have same address as B->base;
 *   char mydata[50];
 *   struct getopts options;
 * };
 *
 */
struct kservice_t {
	uint64_t timestamp;

	/**
	 * @brief Setting to non-zero will shut down the event loop
	 *
	 */
	uint8_t should_run;

	/**
	 * @brief Makes it easy to plug different transports in
	 *
	 */
	knet2_ip_handler_t *network_handler;

	size_t client_size;

	/**
	 * @brief A list of clients
	 *
	 */
	khashmap_t *clients;


	/**
	 * @brief TCP has a timeout, others may not
	 *
	 */
	uint64_t network_timeout_seconds;
	uint64_t kservice_client_timeout_seconds;
	uint64_t last_client_timeout_time;


	//TODO: This will replace handle_client
	kservice_handle_io_event_f handle_io_event;

	klist_t loop_complete_callbacks;
};

//time functions
uint64_t kservice_timestamp();
#define KSERVICE_TIME_DIFF(t1, t2) t1 - t2
#define KSERVICE_TIME_SECONDS(t) t


void kservice_init(kservice_t *service, knet2_ip_handler_t *net, size_t client_size, uint64_t network_timeout_seconds );
int kservice_start(kservice_t* service, knet2_error_t *error);
void kservice_shutdown_frees(kservice_t* service);

void kservice_register_client_allocs(kservice_t* service, kservice_client_t* client);
void kservice_remove_client_frees(kservice_t *service, kservice_client_t *addr);

void kservice_loop(kservice_t* service, knet2_error_t *error);


kservice_client_t *kservice_connect_allocs(kservice_t *service, const char *host, knet2_error_t *error);

kservice_client_t *kservice_find_client_by_sockfd(kservice_t *service, int target_sockfd);


void kservice_timeout_clients(kservice_t *service);

void kservice_accept_client(kservice_t *service, knet2_error_t *error);
int kservice_write_client(kservice_t *service, kservice_client_t *client, knet2_error_t *error);
void kservice_destroy_client_frees(kservice_t *service, kservice_client_t *client);
void kservice_queue_write_allocs(kservice_t *service, kservice_client_t *client, uint8_t *data, size_t len);
void kservice_add_loop_complete_callback(kservice_t *service, void *context, kservice_handle_loop_iteration_f cb);
#endif
