/**
 * @file knet2_select.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Easy tcp funcs for net
 *
 * Tcp implementation of knet2 @@.
 * @see https://klib.gitlab.io/libknet/
 */
#ifndef KNET2_SELECT
#define KNET2_SELECT
#include <khashmap.h>
#include <knet2.h>


typedef struct knet2_select_handler_t {
	knet2_ip_handler_t ip_base;

	khashmap_t *watched_fds;
	fd_set send_fd_set;
	fd_set recv_fd_set;
	/*
	 *  FD_SETSIZE,  on
	 *       the  range  of  file  descriptors  that can be specified in a file descriptor set.  The Linux kernel
	 *       imposes no fixed limit, but the glibc implementation makes fd_set a fixed-size type, with FD_SETSIZE
	 *       defined  as 1024, and the FD_*() macros operating according to that limit.
	 *
	 *   To monitor file descriptors greater than 1023, use poll(2) instead.
	 *
	 */
} knet2_select_handler_t;

/**
 * @brief Initializes the tcp handler
 *
 * @param tcp p_tcp:...
 * @param error p_error:...
 */
int knet2_select_init(knet2_select_handler_t *tcp, knet2_error_t *error);

/**
 * @brief Determines clients ready to send, write or with exception.
 *
 * @param vnet p_vnet:...
 * @param clients p_clients:...
 * @param ready_read_list p_ready_read_list:...
 * @param ready_write_list p_ready_write_list:...
 * @param exception_list p_exception_list:...
 * @return int
 */
int knet2_select_wait(knet2_ip_handler_t *vnet, knet2_event_t *events, size_t max_events, knet2_error_t *error);

int knet2_select_add_watched_addr(knet2_ip_handler_t *ip, knet2_ip_addr_t *addr);
int knet2_select_mod_watched_addr(knet2_ip_handler_t *ip, knet2_ip_addr_t *addr, uint32_t flags);
int knet2_select_del_watched_addr(knet2_ip_handler_t *ip, knet2_ip_addr_t *addr);

void knet2_select_release_frees(knet2_ip_handler_t *vnet);
#endif
