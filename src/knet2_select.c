#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

#include <knet2_select.h>


int knet2_select_add_watched_addr(knet2_ip_handler_t *ip, knet2_ip_addr_t *addr)
{
	if (addr->sockfd > 1023) {
		return -1;
	}
	knet2_select_handler_t *tcp = (knet2_select_handler_t*)ip;
	khashmap_key_t key = { 0 };
	key.data = (uint8_t*)&addr->sockfd;
	key.len = sizeof(int);

	khashmap_put_allocs(tcp->watched_fds, &key, &addr->sockfd);

	FD_SET(addr->sockfd, &tcp->recv_fd_set);
	return 0;
}
int knet2_select_mod_watched_addr(knet2_ip_handler_t *ip, knet2_ip_addr_t *addr, uint32_t flags)
{
	if (addr->sockfd > 1023) {
		return -1;
	}

	knet2_select_handler_t *tcp = (knet2_select_handler_t*)ip;

	if (flags > 0) {
		FD_SET(addr->sockfd, &tcp->send_fd_set);
	} else {
		FD_CLR(addr->sockfd, &tcp->send_fd_set);
	}

	return 0;
}
int knet2_select_del_watched_addr(knet2_ip_handler_t *ip, knet2_ip_addr_t *addr)
{
	if (addr->sockfd > 1023) {
		return -1;
	}

	knet2_select_handler_t *tcp = (knet2_select_handler_t*)ip;

	FD_CLR(addr->sockfd, &tcp->recv_fd_set);
	FD_CLR(addr->sockfd, &tcp->send_fd_set);

	khashmap_key_t key = { 0 };
	key.data = (uint8_t*)&addr->sockfd;
	key.len = sizeof(int);
	khashmap_remove_frees(tcp->watched_fds, &key);

	return 0;

}

void knet2_select_release_frees(knet2_ip_handler_t *vnet)
{
	knet2_select_handler_t *net = (knet2_select_handler_t*)vnet;

	khashmap_release_frees(net->watched_fds);
}

int knet2_select_init(knet2_select_handler_t *tcp, knet2_error_t *error)
{

	bzero(tcp, sizeof(knet2_select_handler_t));

	if (knet2_ip_init(&tcp->ip_base, error)) {
		return -1;
	}
	FD_ZERO(&tcp->send_fd_set);
	FD_ZERO(&tcp->recv_fd_set);
	tcp->ip_base.wait = knet2_select_wait;
	tcp->ip_base.add_watched_addr = knet2_select_add_watched_addr;
	tcp->ip_base.mod_watched_addr = knet2_select_mod_watched_addr;
	tcp->ip_base.del_watched_addr = knet2_select_del_watched_addr;
	tcp->ip_base.release_frees = knet2_select_release_frees;

	//interrupt will not write
	FD_SET(tcp->ip_base.interrupt_fd, &tcp->recv_fd_set);

	tcp->watched_fds = khashmap_new_allocs(1024);
	khashmap_key_t key = { 0 };
	key.data = (uint8_t*)&tcp->ip_base.interrupt_fd;
	key.len = sizeof(int);

	khashmap_put_allocs(tcp->watched_fds, &key, &tcp->ip_base.interrupt_fd);

	return 0;
}

//what is calling wait()
//service is
//it has a list of clients, not addrs.  It would be best to not have to iterate to call wait, or after calling it,
//the best place to iterate is when select returns.
int knet2_select_wait(knet2_ip_handler_t *vnet, knet2_event_t *events, size_t max_events, knet2_error_t *error)
{
	knet2_select_handler_t *net = (knet2_select_handler_t*)vnet;

	struct timeval timeout;

	timeout.tv_sec = 0;
	timeout.tv_usec = net->ip_base.select_timeout_usec;

	fd_set read_fd_set, write_fd_set, exception_fd_set;
	memcpy(&read_fd_set, &net->recv_fd_set, sizeof(read_fd_set));
	memcpy(&exception_fd_set, &net->recv_fd_set, sizeof(read_fd_set));
	memcpy(&write_fd_set, &net->send_fd_set, sizeof(write_fd_set));

	struct timeval *tval = &timeout;
	if (timeout.tv_usec == -1) {
		tval = NULL;
	}

	int result = select(FD_SETSIZE, &read_fd_set, &write_fd_set, &exception_fd_set, tval);
	if (result < 0) {
		result = errno;
		if (result != EWOULDBLOCK && result != EINPROGRESS) {
			knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling select");
			result = -1;
		} else {
			result = 0;
		}
	} else if (result > 0) {
		size_t i = 0;
		khashmap_entry_t *entry = NULL;
		KHASHMAP_FOR_EACH_VALUE(net->watched_fds, entry) {
			int fd = *((int*)entry->value);
			uint32_t flags = 0;


			if (FD_ISSET(fd, &read_fd_set)) {
				flags |= KNET2_EV_IN;
			}
			if (FD_ISSET(fd, &exception_fd_set)) {
				flags |= KNET2_EV_ERR;
			}
			if (FD_ISSET(fd, &write_fd_set)) {
				flags |= KNET2_EV_OUT;
			}
			if (flags) {
				memset(&events[i], 0, sizeof(knet2_event_t));
				events[i].fd = fd;
				i++;
				if (i == result) {
					break;
				}
			}
		}
	}

	return result;
}

