#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/eventfd.h>
#include <stdarg.h>

#include <knet2.h>




void knet2_set_knet2_error(knet2_error_t *error, int errcode, char *proto_error, const char *format, ...)
{
	memset(error, 0, sizeof(knet2_error_t));
	va_list args;
	va_start(args, format);
	vsnprintf(error->knet2_errmsg, sizeof(error->knet2_errmsg), format, args);
	va_end(args);
	error->errcode = errcode;

	if (proto_error != NULL) {
		snprintf(error->proto_errmsg, sizeof(error->proto_errmsg), "%s", proto_error);
	}

}

int knet2_interrupt(knet2_ip_handler_t* net, knet2_error_t *error)
{
	eventfd_t event = { 0 };
	int result = eventfd_write(net->interrupt_fd, event);

	if (result != 8 && errno != EAGAIN && errno != EWOULDBLOCK) {
		knet2_set_knet2_error(error, errno, strerror(errno), "Could not eventfd_write interrupt fd");
		return -1;
	}
	return 0;
}

int knet2_ip_addr_resolve_host(const char *host, knet2_ip_addr_t* dest, knet2_error_t *error)
{
	//http://beej.us/guide/bgnet/html/multi/syscalls.html#getaddrinfo
	int status = -1;
	struct addrinfo hints;
	struct addrinfo *res, *p;               // will point to the results

	memset(&hints, 0, sizeof hints);        // make sure the struct is empty
	hints.ai_family = AF_UNSPEC;            // don't care IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM;        // TCP stream sockets

	if ((status = getaddrinfo(host, NULL, &hints, &res)) != 0) {

		knet2_set_knet2_error(error, 0, NULL, "knet2_ip_addr_resolve_host failed %s", gai_strerror(status));
		return -1;
	}

	// servinfo now points to a linked list of 1 or more struct addrinfos
	for (p = res; p != NULL; p = p->ai_next) {
		// get the pointer to the address itself,
		// different fields in IPv4 and IPv6:
		if (p->ai_family == AF_INET) { // IPv4
			struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
			knet2_ip_addr_t *ip_addr = dest;
			memcpy(&ip_addr->saddr, ipv4, sizeof(struct sockaddr_in));
			status = 0;
			break;
		}
	}


	// ... do everything until you don't need servinfo anymore ....

	freeaddrinfo(res); // free the linked-list
	if (status == -1) {
		knet2_set_knet2_error(error, 0, NULL, "knet2_ip_addr_resolve_host unsupported protocol %s", host);
	}
	return status;
}

int knet2_ip_addr_parse(knet2_ip_addr_t  *vaddr, const char* str_addr, knet2_error_t *error)
{
	memset(vaddr, 0, sizeof(knet2_ip_addr_t));
	knet2_ip_addr_t *new = vaddr;
	knet2_ip_host_port_t host_ip;

	if (knet2_ip_parse_host_port(&host_ip, str_addr)) {
		knet2_set_knet2_error(error, 0, NULL, "knet2_ip_parse_host_port failed %s", str_addr);
		return -2;
	}
	uint16_t us_port = atoi(host_ip.port);
	int result = knet2_ip_addr_resolve_host(host_ip.host, vaddr, error);
	if (result != 0) {
		return -1;
	}
	new->saddr.sin_family = AF_INET;
	new->saddr.sin_port = htons(us_port);

	return 0;
}

int knet2_ip_parse_host_port(knet2_ip_host_port_t *host_ip, const char *str_addr)
{
	size_t str_len = strlen(str_addr);

	size_t len = strcspn(str_addr, ":");

	if (len >= str_len) {
		return -1;
	}
	const char *strport = str_addr + len + 1;// not len-1, to advance past ":"
	size_t port_str_len = strlen(strport);
	if (port_str_len == 0) {
		return -2;
	}

	for (int i = 0; i <  port_str_len; i++ ) {
		if (strport[i] < '0' || strport[i] > '9') {
			return -3;
		}
	}

	if (len > sizeof(host_ip->host) - 1) {
		return -4;
	} else if (port_str_len > sizeof(host_ip->port) - 1) {
		return -5;
	}

	uint32_t port = atoi(strport);
	if (port > (uint32_t)0xffff) {
		return -6;
	}
	memset(host_ip, 0, sizeof(knet2_ip_host_port_t));
	snprintf(host_ip->port, sizeof(host_ip->port), "%s", strport);
	snprintf(host_ip->host, len + 1, "%s", str_addr);//len because we don't want ":port"
	return 0;
}

void knet2_ip_release_addr(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr)
{
	knet2_ip_addr_t *ip = addr;

	if (vnet->del_watched_addr != NULL) {
		vnet->del_watched_addr(vnet, addr);
	}

	if (ip->sockfd > 0) {
		shutdown(ip->sockfd, SHUT_RDWR);
		close(ip->sockfd);
		ip->sockfd = 0;
	}
}

int knet2_ip_address_reverse(knet2_ip_addr_t *vaddr, char *buffer, size_t buflen, knet2_error_t *error)
{
	knet2_ip_addr_t *addr = vaddr;
	char *tmpbuff = buffer;
	const char * ptr = inet_ntop( AF_INET, &(addr->saddr.sin_addr), tmpbuff, buflen);

	if (ptr != tmpbuff) {
		knet2_set_knet2_error(error, errno, strerror(errno), "inet_ntop failed");
		return -1;
	}
	buffer += strlen(ptr);
	buflen -= strlen(ptr);
	if (buflen < 5) {
		knet2_set_knet2_error(error, 0, NULL, "buffer was too small to reverse port portion of address");
		return -1;
	}
	snprintf(buffer, buflen, ":%u", ntohs(addr->saddr.sin_port) );
	return 0;
}



int knet2_ip_init(knet2_ip_handler_t *vnet, knet2_error_t *error)
{
	memset(vnet, 0, sizeof(knet2_ip_handler_t));
	knet2_ip_handler_t *handler = vnet;

	handler->sizeof_addr = sizeof(knet2_ip_addr_t);
	handler->listen_backlog = 255;
	handler->addr_parse = knet2_ip_addr_parse;
	handler->addr_reverse = knet2_ip_address_reverse;
	handler->release_addr = knet2_ip_release_addr;
	handler->listen_backlog = 255;
	handler->accept = knet2_ip_accept;
	handler->listen = knet2_ip_listen;
	handler->connect = knet2_ip_connect;
	handler->recv = knet2_ip_recv;
	handler->send = knet2_ip_send;


	sigset_t mask = { 0 };

	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);

	handler->interrupt_fd = eventfd(0, EFD_NONBLOCK);
	if (handler->interrupt_fd == -1) {
		knet2_set_knet2_error(error, -3, strerror(errno), "Could not create interrupt fd");
		return -3;
	}

	return 0;
}

int knet2_ip_set_socket_options(int sockfd, knet2_error_t *error)
{
	int int_val_on = 1;
	int result = 0;

	result = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, (const char*)&int_val_on, sizeof(int_val_on));
	if (result < 0) {
		knet2_set_knet2_error(error, errno, strerror(errno), "setsockopt(SO_REUSEADDR) failed");
		return -2;
	}

	return 0;
}


int knet2_ip_accept(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli, knet2_error_t *error)
{
	knet2_ip_handler_t *net = (knet2_ip_handler_t*)vnet;
	knet2_ip_addr_t *serv = &net->this_addr;
	knet2_ip_addr_t *cli = vcli;
	uint32_t addrlen = sizeof(serv->saddr);

	cli->sockfd = accept(serv->sockfd, (struct sockaddr *)&cli->saddr, &addrlen);
	if (cli->sockfd < 0) {
		int result = errno;
		if (result != EWOULDBLOCK) {
			knet2_set_knet2_error(error, errno, strerror(errno), "ERROR on accept");
			return -1;
		} else {
			return 0;
		}
	}

	int result = knet2_ip_set_socket_options(cli->sockfd, error);
	if (result < 0) {
		return -2;
	}
	if (vnet->add_watched_addr != NULL && vnet->add_watched_addr(vnet, vcli)) {
		return -1;
	}
	return 1;
}

int knet2_ip_listen(knet2_ip_handler_t *vnet, knet2_error_t *error)
{
	//the only cast needed (I think)
	knet2_ip_handler_t *net = (knet2_ip_handler_t*)vnet;
	knet2_ip_addr_t *serv = &net->this_addr;

	serv->sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (serv->sockfd < 0) {
		knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling socket");
		return -1;
	}
	int result = knet2_ip_set_socket_options(serv->sockfd, error);
	if (result < 0) {
		return -2;
	}
	char char_val_on = 1;
	result = ioctl(serv->sockfd, FIONBIO, (char*)&char_val_on);
	if (result < 0) {
		knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling ioctl(FIONBIO)");
		return -3;
	}
	result = bind(serv->sockfd, (struct sockaddr *)&serv->saddr, sizeof(serv->saddr));
	if (result < 0) {
		knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling bind");
		return -4;
	}
	result = listen(serv->sockfd, net->listen_backlog);
	if (result != 0) {
		knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling listen");
		return -5;
	}
	if (vnet->add_watched_addr != NULL && vnet->add_watched_addr(vnet, serv)) {
		return -1;
	}
	return 0;
}


int knet2_ip_connect(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr, knet2_error_t *error)
{
	knet2_ip_addr_t *cli = addr;

	cli->sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (cli->sockfd < 0) {
		knet2_set_knet2_error(error, errno, strerror(errno), "ERROR in connect() calling socket");
		return -1;
	}

	int result;

	result = knet2_ip_set_socket_options(cli->sockfd, error);
	if (result < 0) {
		return -2;
	}

	result = connect(cli->sockfd, (const struct sockaddr*)&cli->saddr, sizeof(cli->saddr));
	if (result != 0  && errno != EINPROGRESS ) {
		knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling connect");
		return -3;
	}

	if (vnet->add_watched_addr != NULL && vnet->add_watched_addr(vnet, cli)) {
		return -1;
	}

	return 0;
}

size_t knet2_ip_recv(knet2_ip_handler_t* vnet, knet2_ip_addr_t* vaddr, uint8_t *buffer, size_t buffer_size, knet2_error_t *error)
{
	knet2_ip_addr_t *ip = vaddr;
	int n = recv(ip->sockfd, buffer, buffer_size, MSG_DONTWAIT);

	if (n < 0) {
		n = errno;
		if (n != EWOULDBLOCK && n != EINPROGRESS) {
			knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling recv");
			return -1;
		} else {
			return 0;
		}
	}
	return n;
}


size_t knet2_ip_send(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vaddr, uint8_t *buffer, size_t buffer_size, knet2_error_t *error)
{
	knet2_ip_addr_t *ip = vaddr;

	int n = send(ip->sockfd, buffer, buffer_size, 0);

	if (n < 0) {
		knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling send");
		return -1;
	}

	return n;
}
