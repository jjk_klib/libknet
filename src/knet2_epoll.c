#include <string.h>
#include <strings.h>
#include <sys/epoll.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/eventfd.h>


#include "knet2_epoll.h"

//writing needs to maintain a buffer of unwritten, and then use EAGAIN + epoll_ctl MOD and add EPOLLOUT until it's done writing.
int knet2_epoll_add_watched_addr(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr)
{
	knet2_epoll_handler_t *net = (knet2_epoll_handler_t*)vnet;
	struct epoll_event event = { 0 };

	event.events = EPOLLIN | EPOLLERR;//  & EPOLLET;
	event.data.fd = addr->sockfd;
	//event.data.ptr = addr; This is *NOT* what I thought it was...
	if (epoll_ctl(net->epoll_fd, EPOLL_CTL_ADD, event.data.fd, &event)) {
		return -1;
	}
	return 0;
}

int knet2_epoll_mod_watched_addr(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr, uint32_t flags)
{
	knet2_epoll_handler_t *net = (knet2_epoll_handler_t*)vnet;
	struct epoll_event event = { 0 };

	event.events = EPOLLIN | EPOLLERR;
	if (flags != 0) {
		event.events = event.events | EPOLLOUT;
	}

	event.data.fd = addr->sockfd;

	if (epoll_ctl(net->epoll_fd, EPOLL_CTL_MOD, event.data.fd, &event)) {
		return -1;
	}
	return 0;
}

int knet2_epoll_del_watched_addr(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr)
{
	knet2_epoll_handler_t *net = (knet2_epoll_handler_t*)vnet;
	struct epoll_event event = { 0 };

	event.events = EPOLLIN | EPOLLERR | EPOLLOUT;//  & EPOLLET;
	event.data.fd = addr->sockfd;

	if (epoll_ctl(net->epoll_fd, EPOLL_CTL_DEL, event.data.fd, &event)) {
		return -1;
	}
	return 0;
}

int knet2_epoll_wait(knet2_ip_handler_t *vnet, knet2_event_t *events, size_t max_events, knet2_error_t *error)
{
	knet2_epoll_handler_t *net = (knet2_epoll_handler_t*)vnet;

	//lower of the two - do not get more events than net supports
	if (max_events > net->max_events) {
		max_events = net->max_events;
	}
	int num_ready = epoll_wait(net->epoll_fd, net->events, max_events, net->ip_base.select_timeout_usec * 1000);

	if (num_ready < 0) {
		knet2_set_knet2_error(error, errno, strerror(errno), "ERROR calling knet2_epoll_wait");
		return -1;
	}
	for (int i = 0; i < num_ready; i++) {
		struct epoll_event *ev = &net->events[i];
		memset(&events[i], 0, sizeof(knet2_event_t));
		events[i].fd = ev->data.fd;
		if (ev->events & EPOLLIN) {
			events[i].flags |= KNET2_EV_IN;
		}
		if (ev->events & EPOLLERR) {
			events[i].flags |= KNET2_EV_ERR;
		}
		if (ev->events & EPOLLOUT) {
			events[i].flags |= KNET2_EV_OUT;
		}
	}
	return num_ready;
}

int knet2_epoll_handler_init_allocs(knet2_epoll_handler_t *unintialized_net, int max_events, knet2_error_t *error)
{
	bzero(unintialized_net, sizeof(knet2_epoll_handler_t));
	knet2_ip_init(&unintialized_net->ip_base, error);

	knet2_epoll_handler_t *net = unintialized_net;
	net->epoll_fd = epoll_create1(0);
	if (net->epoll_fd == -1) {
		knet2_set_knet2_error(error, -1, strerror(errno), "Could not create epoll handler");
		return -1;
	}

	//this doesn't have a addr so I just add it directly
	struct epoll_event event = { 0 };

	event.events = EPOLLIN & EPOLLERR;;
	event.data.fd = net->ip_base.interrupt_fd;
	if (epoll_ctl(net->epoll_fd, EPOLL_CTL_ADD, event.data.fd, &event)) {
		knet2_set_knet2_error(error, -4, strerror(errno), "Could not set ctl on interrupt fd");
		return -4;
	}

	net->max_events = max_events;
	net->events = calloc(sizeof(struct epoll_event), net->max_events);
	net->ip_base.wait = knet2_epoll_wait;
	net->ip_base.release_addr = knet2_epoll_release_addr;


	net->ip_base.add_watched_addr = knet2_epoll_add_watched_addr;
	net->ip_base.mod_watched_addr = knet2_epoll_mod_watched_addr;
	net->ip_base.del_watched_addr = knet2_epoll_del_watched_addr;
	net->ip_base.release_frees = knet2_epoll_handler_release_frees;
	return 0;
}

void knet2_epoll_handler_release_frees(knet2_ip_handler_t *vnet)
{
	knet2_epoll_handler_t *net = (knet2_epoll_handler_t*)vnet;

	if (net->events) {
		free(net->events);
		net->events = 0;
	}
	net->max_events = 0;
	if (net->epoll_fd > 0) {
		close(net->epoll_fd);
		net->epoll_fd = 0;
	}
	if (net->ip_base.this_addr.sockfd  > 0) {
		close(net->ip_base.this_addr.sockfd );
		net->ip_base.this_addr.sockfd = 0;
	}
	if (net->ip_base.interrupt_fd > 0) {
		close(net->ip_base.interrupt_fd);
		net->ip_base.interrupt_fd = 0;
	}
}

void knet2_epoll_release_addr(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vaddr)
{
	knet2_ip_release_addr(vnet, vaddr);
	knet2_epoll_handler_t *net = (knet2_epoll_handler_t*)vnet;

	struct epoll_event event = { 0 };
	knet2_ip_addr_t *addr = vaddr;

	event.events = EPOLLIN | EPOLLERR;
	event.data.fd = addr->sockfd;
	epoll_ctl(net->epoll_fd, EPOLL_CTL_DEL, event.data.fd, &event);
}
