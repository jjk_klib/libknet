#include <kservice.h>
#include <klist.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/time.h>

typedef struct {
	void *context;
	kservice_handle_loop_iteration_f callback;
} kservice_callback_t;
void kservice_add_loop_complete_callback(kservice_t *service, void *context, kservice_handle_loop_iteration_f callback)
{
	kservice_callback_t *cb = (kservice_callback_t*)calloc(1, sizeof(kservice_callback_t));

	cb->context = context;
	cb->callback = callback;
	klist_insert_value_allocs(&service->loop_complete_callbacks, NULL, cb);
}
uint64_t kservice_timestamp()
{
	struct timeval tv1;

	// get the first time:
	gettimeofday(&tv1, NULL);
	return tv1.tv_sec;
}

void kservice_init(kservice_t *service, knet2_ip_handler_t *net, size_t client_size, uint64_t network_timeout_seconds)
{
	service->network_handler = net;
	service->client_size = client_size;
	service->network_timeout_seconds = network_timeout_seconds;
	service->kservice_client_timeout_seconds = 1;
	service->clients = khashmap_new_allocs(1024);
}

int kservice_start(kservice_t* service, knet2_error_t *error)
{
	int result = service->network_handler->listen(service->network_handler, error);

	if (result != 0) {
		return -1;
	}
	service->should_run = 1;
	return 0;
}

void kservice_shutdown_frees(kservice_t* service)
{
	service->should_run = 0;
	//free should be done by caller as we don't know what's passed to us (ref or ptr)

    if (service->network_handler) {
        service->network_handler->release_addr(service->network_handler, &service->network_handler->this_addr);
    }

	khashmap_entry_t removed = { 0 };
	khashmap_entry_t *entry = NULL;
    if (service->clients) {
        KHASHMAP_FOR_EACH_VALUE(service->clients, entry) {
            removed.node.next = entry->node.next;
            kservice_client_t *client = entry->value;
            kservice_destroy_client_frees(service, client);
            //don't lose our place, this is annoying as f
            entry = &removed;
        }
        khashmap_release_frees(service->clients);
        service->clients=NULL;
    }
	KLIST_FOR_EACH_NODE(node, &service->loop_complete_callbacks) {
		free(node->value);
	}
	klist_release_frees(&service->loop_complete_callbacks);
    if (service->network_handler) {
        service->network_handler->release_frees(service->network_handler);
    }
    service->network_handler = NULL;
}

void kservice_register_client_allocs(kservice_t* service, kservice_client_t* client)
{
	khashmap_key_t key = { 0 };

	key.data = (uint8_t*)&client->addr->sockfd;
	key.len = sizeof(int);
	khashmap_put_allocs(service->clients, &key, client);
}

/**
 * caller must handle freeing client or client.addr.
 */
void kservice_remove_client_frees(kservice_t *service, kservice_client_t* client)
{
	khashmap_key_t key = { 0 };

	key.data = (uint8_t*)&client->addr->sockfd;
	key.len = sizeof(int);
	khashmap_remove_frees(service->clients, &key);

	if (client->addr) {
		service->network_handler->release_addr(service->network_handler, client->addr);
	}
	while (client->write_buffers.head != NULL) {
		klist_node_t *head = client->write_buffers.head;
		kservice_write_buffer_t *buff = (kservice_write_buffer_t*)head->value;
		klist_remove_node(&client->write_buffers, head);
		free(head);
		free(buff);
	}
}

kservice_client_t *kservice_find_client_by_sockfd(kservice_t *service, int target_sockfd)
{
	khashmap_key_t key = { 0 };

	key.data = (uint8_t*)&target_sockfd;
	key.len = sizeof(int);
	return khashmap_get(service->clients, &key);
}

void kservice_accept_client(kservice_t *service, knet2_error_t *error)
{
	knet2_ip_handler_t *net = service->network_handler;
	kservice_client_t *client = malloc(service->client_size);

	memset(client, 0, service->client_size);
	client->addr = malloc(service->network_handler->sizeof_addr);
	memset(client->addr, 0, service->network_handler->sizeof_addr);

	if (net->accept(net, client->addr, error) == 1) {
		client->last_seen_time = service->timestamp;
		kservice_register_client_allocs(service, client);
		client = NULL;
	} else {
		//this should not happen or wait lies.
		free(client);
	}
}

//This queues/buffers the write and sets fd to be watched for write flag.
void kservice_queue_write_allocs(kservice_t *service, kservice_client_t *client, uint8_t *data, size_t len)
{
	kservice_write_buffer_t *wb = calloc(1, sizeof(kservice_write_buffer_t) + len);

	wb->size = len;
	memcpy(wb->data, data, len);
	klist_insert_value_allocs(&client->write_buffers, NULL, wb);
	service->network_handler->mod_watched_addr(service->network_handler, client->addr, KNET2_EV_OUT);
}

//This is the one actually sending the data, not buffering.
int kservice_write_client(kservice_t *service, kservice_client_t *client, knet2_error_t *error)
{
	if (client->write_buffers.count > 0) {
		klist_node_t *head = client->write_buffers.head;
		kservice_write_buffer_t *buffer = (kservice_write_buffer_t*)head->value;
		if (buffer->index < buffer->size) {
			uint8_t *data = buffer->data + buffer->index;
			size_t len = buffer->size - buffer->index;
			int n = service->network_handler->send(service->network_handler, client->addr, data, len, error);
			if (n < 0) {
				return -1;
			} else if (n == len) {
				klist_remove_node(&client->write_buffers, head);
				free(head);
				free(buffer);
				if (client->write_buffers.count == 0) {
					service->network_handler->mod_watched_addr(service->network_handler, client->addr, 0);
				}
				return 0;
			} else {
				buffer->index += n;
				return len - n;
			}
		}
	}
	return 0;
}

void kservice_destroy_client_frees(kservice_t *service, kservice_client_t *client)
{
	kservice_remove_client_frees(service, client);
	free(client->addr);
	free(client);
}

void kservice_loop(kservice_t* service, knet2_error_t *error)
{
	knet2_ip_handler_t *net = service->network_handler;

	const size_t MAX_EVENTS = 1024;
	knet2_event_t events[MAX_EVENTS];

	while (service->should_run) {
		service->timestamp = kservice_timestamp();

		int result = net->wait(net, events, MAX_EVENTS, error);

		if (result < 0) {
			result = -1;
		} else {
			for (int i = 0; i < result; i++) {
				knet2_event_t *ev = &events[i];
				if (ev->fd == net->interrupt_fd) {
					//preempt other events and interrupt.
					break;
				} else if (ev->fd == net->this_addr.sockfd) {
					//accept will get called after other events.
					kservice_accept_client(service, error);
				} else {
					//not interrupt, not listener, just a client
					kservice_client_t *c = kservice_find_client_by_sockfd(service, ev->fd);
					if (c == NULL) {
						continue;
					}
					service->handle_io_event(service, c, ev);
				}
			}
			uint64_t timediff = KSERVICE_TIME_DIFF(service->timestamp, service->last_client_timeout_time);
			if (timediff > KSERVICE_TIME_SECONDS(service->kservice_client_timeout_seconds)) {
				kservice_timeout_clients(service);
			}
		}
		KSERVICE_UNLOCK(mutex);
		KLIST_FOR_EACH_NODE(node, &service->loop_complete_callbacks) {
			kservice_callback_t *cb = (kservice_callback_t*)node->value;

			cb->callback(service, cb->context);
		}
	}
}

void kservice_timeout_clients(kservice_t *service)
{
	khashmap_entry_t removed = { 0 };
	khashmap_entry_t *entry = NULL;

	KHASHMAP_FOR_EACH_VALUE(service->clients, entry) {
		kservice_client_t *client = (kservice_client_t*)entry->value;
		uint64_t timediff = KSERVICE_TIME_DIFF(service->timestamp, client->last_seen_time);

		if (KSERVICE_TIME_SECONDS(timediff) > service->network_timeout_seconds) {
			removed.node.next = entry->node.next;
			kservice_destroy_client_frees(service, client);
			//prevents a skip... :/
			entry = &removed;
		}
	}
}

kservice_client_t *kservice_connect_allocs(kservice_t *service, const char *host, knet2_error_t *error)
{
	kservice_client_t *client = calloc(1, service->client_size);

	client->addr = calloc(1, service->network_handler->sizeof_addr);
	client->last_seen_time = service->timestamp;

	int result = service->network_handler->addr_parse(client->addr, host, error);
	if (result) {
		free(client);//there is nothing to remove/release here.
		client = 0;
	} else {
		knet2_ip_handler_t *net = service->network_handler;
		if (net->connect(net, client->addr, error)) {
			kservice_destroy_client_frees(service, client);
			client = NULL;
		} else {
			kservice_register_client_allocs(service, client);
		}
	}
	return client;
}
