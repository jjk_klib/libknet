#include "tests.h"

extern "C" {
#include <knet2_epoll.h>
#include <stdio.h>
}

const struct epoll_event *epoll_event_NULL = NULL;
TEST(knet2_epoll, knet2_epoll_handler_init_allocs) {
	knet2_epoll_handler_t net = { };
	knet2_error_t error = { };

	ASSERT_EQ(knet2_epoll_handler_init_allocs(&net, 10, &error), 0);
	ASSERT_GT(net.epoll_fd, 0);
	ASSERT_EQ(net.max_events, 10);
	ASSERT_NE(net.events, epoll_event_NULL);
	knet2_epoll_handler_release_frees((knet2_ip_handler_t*)&net);

	ASSERT_EQ(net.epoll_fd, 0);
	ASSERT_EQ(net.max_events, 0);
	ASSERT_EQ(net.events, epoll_event_NULL);

}

TEST(knet2_epoll, knet2_epoll_wait__zero_after_accept){
	const char *addr = "127.0.0.1:60004";
	knet2_epoll_handler_t net = { };
	knet2_error_t error = { };

	ASSERT_EQ(knet2_epoll_handler_init_allocs(&net, 10, &error), 0);
	knet2_ip_addr_parse(&net.ip_base.this_addr, addr, &error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &error);
	ASSERT_EQ(0, result);

	knet2_ip_addr_t srv;
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&srv), addr, &error);
	result = knet2_ip_connect((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&srv, &error);
	ASSERT_EQ(0, result);


	knet2_ip_addr_t cli;
	memset(&cli, 0, sizeof(knet2_ip_addr_t));
	result = knet2_ip_accept((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli, &error);
	ASSERT_EQ(1, result);

	const size_t MAX_EVENTS = 1024;
	knet2_event_t events[MAX_EVENTS];
	result = knet2_epoll_wait((knet2_ip_handler_t*)&net,  events, MAX_EVENTS, &error);
	ASSERT_EQ(0, result);

	knet2_epoll_release_addr((knet2_ip_handler_t*)&net, &net.ip_base.this_addr);
	knet2_epoll_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&srv));
	knet2_epoll_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli);

	knet2_epoll_handler_release_frees((knet2_ip_handler_t*)&net);
}

TEST(knet2_epoll, knet2_epoll_wait__read_ready){
	const char *addr = "127.0.0.1:60004";
	knet2_epoll_handler_t net = { };
	knet2_error_t error = { };

	ASSERT_EQ(knet2_epoll_handler_init_allocs(&net, 10, &error), 0);
	knet2_ip_addr_parse(&net.ip_base.this_addr, addr, &error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &error);
	ASSERT_EQ(0, result);

	knet2_ip_addr_t srv;
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&srv), addr, &error);
	result = knet2_ip_connect((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&srv, &error);
	ASSERT_EQ(0, result);


	knet2_ip_addr_t cli;
	memset(&cli, 0, sizeof(knet2_ip_addr_t));

	net.ip_base.select_timeout_usec = 0;
	result = knet2_ip_accept((knet2_ip_handler_t*)&net, &cli, &error);
	ASSERT_EQ(1, result);

	const size_t MAX_EVENTS = 1024;
	knet2_event_t events[MAX_EVENTS];
	result = knet2_epoll_wait((knet2_ip_handler_t*)&net, events, MAX_EVENTS, &error);
	ASSERT_EQ(result, 0);


	net.ip_base.select_timeout_usec = 0;
	char buffer[12] = "test";
	knet2_ip_send((knet2_ip_handler_t*)&net, &srv, (uint8_t*)buffer, sizeof(buffer), &error);
	result = -1;
	int count = 0;
	while (result != 1 && count++ < 5) {
		sleep(1);
		result = knet2_epoll_wait((knet2_ip_handler_t*)&net, events, MAX_EVENTS, &error);
	}
	ASSERT_EQ(result, 1);
	ASSERT_EQ(cli.sockfd, events[0].fd);
	ASSERT_EQ(KNET2_EV_IN, events[0].flags);

	knet2_epoll_release_addr((knet2_ip_handler_t*)&net, &net.ip_base.this_addr);
	knet2_epoll_release_addr((knet2_ip_handler_t*)&net, &srv);
	knet2_epoll_release_addr((knet2_ip_handler_t*)&net, &cli);

	knet2_epoll_handler_release_frees((knet2_ip_handler_t*)&net);
}
