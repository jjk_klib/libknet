#include "tests.h"
extern "C" {
#include <knet2.h>
#include <knet2_select.h>
#include "kservice.h"
}

static knet2_error_t knet2error;
static knet2_select_handler_t tcp;

void test_kservice_init(kservice_t *service, const char *host_ip)
{

	service->client_size = sizeof(kservice_client_t);
	knet2_error_t error = { 0 };

	memset((void*)(&tcp), 0, sizeof(knet2_select_handler_t));
	ASSERT_EQ(knet2_select_init(&tcp, &error), 0);



	memset((void*)service, 0, sizeof(kservice_t));
	kservice_init(service, (knet2_ip_handler_t*)&tcp, sizeof(kservice_client_t), 1);
	create_tcp_addr(&tcp.ip_base.this_addr, host_ip);
}

char remote[KSERVICE_MAX_ADDR_LENGTH];

void test_kservice_handle_io_event_f(kservice_t *service, kservice_client_t *client, knet2_event_t *event)
{
	memset(remote, 0, sizeof(remote));
	snprintf(remote, sizeof(remote), "%s", "test");
}

int test_kservice_mock_knet2_tcp_connect(knet2_ip_handler_t *vnet, knet2_ip_addr_t *addr, knet2_error_t *error)
{
	return 0;
}

TEST(kservice, start){
	kservice_t service;
	const char *host_ip = "127.0.0.1:60004";

	test_kservice_init(&service, host_ip);


	kservice_start(&service, &knet2error);
	ASSERT_EQ(1, service.should_run) <<  "should_run==1";
	knet2_ip_addr_t *check = &service.network_handler->this_addr;
	ASSERT_TRUE(check->sockfd > 0) << "sockfd>0";
	service.network_handler->release_addr(service.network_handler, &service.network_handler->this_addr);
	kservice_shutdown_frees(&service);
}


TEST(kservice, kservice_shutdown_frees){
	kservice_t service;
	const char *host_ip = "127.0.0.1:60005";

	test_kservice_init(&service, host_ip);

	service.should_run = 1;
	kservice_shutdown_frees(&service);
	ASSERT_EQ(0, service.should_run);
}


TEST(kservice, register_new_client){
	kservice_t service;
	const char *host_ip = "127.0.0.1:60006";

	test_kservice_init(&service, host_ip);



	kservice_client_t *container = (kservice_client_t*)calloc(1, sizeof(kservice_client_t));
	container->addr = (knet2_ip_addr_t*)calloc(1, sizeof(knet2_ip_addr_t));
	create_tcp_addr(container->addr, "0.0.0.0:123");
	kservice_register_client_allocs(&service, container);
	ASSERT_EQ(1, service.clients->total_entries);

	kservice_shutdown_frees(&service);
}

TEST(kservice, remove_client_addr){
	kservice_t service;
	const char *host_ip = "127.0.0.1:60007";

	test_kservice_init(&service, host_ip);

	kservice_client_t *container = (kservice_client_t*)calloc(1, sizeof(kservice_client_t));
	container->addr = (knet2_ip_addr_t*)calloc(1, sizeof(knet2_ip_addr_t));
	create_tcp_addr(container->addr, "0.0.0.0:123");

	kservice_register_client_allocs(&service, container);
	ASSERT_EQ(1, service.clients->total_entries);


	kservice_remove_client_frees(&service, container);
	free(container->addr);
	free(container);
	ASSERT_EQ(0, service.clients->total_entries);

	kservice_shutdown_frees(&service);
}


void test_kservice_handle_loop_iteration_f(kservice_t *service, void *context)
{
	//context=null ok for this
	service->should_run = 0;
}

kservice_client_t *test_kservice_get_first_client(khashmap_t *map)
{
	khashmap_entry_t *entry = NULL;

	KHASHMAP_FOR_EACH_VALUE(map, entry) {
		return (kservice_client_t*)entry->value;
	}
	return NULL;
}

TEST(kservice, service_loop){
	kservice_t service;
	const char *host_ip = "127.0.0.1:60009";

	test_kservice_init(&service, host_ip);

	service.handle_io_event = test_kservice_handle_io_event_f;
	kservice_add_loop_complete_callback(&service, NULL, test_kservice_handle_loop_iteration_f);

	kservice_start(&service, &knet2error);


	knet2_ip_handler_t *net = service.network_handler;
	int result = 0;
	knet2_ip_addr_t srv = { 0 };
	create_tcp_addr(&srv, host_ip);

	//connect to itself
    result = net->connect((knet2_ip_handler_t*)net, (knet2_ip_addr_t*)&srv, &knet2error);
	ASSERT_EQ(0, result);
	size_t dbuflen = 128;
	char dbuffer[128] = "Hello there!";
	//talk to itself

    int n = net->send((knet2_ip_handler_t*)net, (knet2_ip_addr_t*)&srv, (uint8_t*)dbuffer, dbuflen, &knet2error);

	//0 doesn't seem so awesome at this point.  intention was pass or fail, not between.
	ASSERT_EQ(dbuflen, n);

	kservice_loop(&service, &knet2error);
	ASSERT_EQ(1, service.clients->total_entries);
	kservice_client_t *tc = test_kservice_get_first_client(service.clients);
	ASSERT_NE((kservice_client_t*)NULL, tc);

    net->release_addr(net, &srv);
	kservice_shutdown_frees(&service);
}

TEST(kservice, connect){
	kservice_t service;
	const char *host_ip = "127.0.0.1:60009";

	test_kservice_init(&service, host_ip);
	service.network_handler->connect = test_kservice_mock_knet2_tcp_connect;
	kservice_client_t *client = kservice_connect_allocs(&service,  host_ip, &knet2error);
	ASSERT_NE(client, (kservice_client_t*)NULL);
	ASSERT_NE(client->addr, (knet2_ip_addr_t*)NULL);
	ASSERT_EQ(service.clients->total_entries, 1);
	kservice_client_t *value = test_kservice_get_first_client(service.clients);
	ASSERT_EQ(value, client);

	kservice_shutdown_frees(&service);
}

TEST(kservice, kservice_shutdown_frees_zeroed) {
    kservice_t service;
    bzero(&service, sizeof(kservice_t));
    kservice_shutdown_frees(&service);
}


TEST(kservice, kservice_time) {
	ASSERT_EQ(0, 0);
}

TEST(kservice, kservice_time_diff) {
	ASSERT_EQ(0, 0);
}

TEST(kservice, kservice_queue_write_allocs) {

}

TEST(kservice, kservice_add_loop_complete_callback) {

}

TEST(kservice, kservice_write_client) {

}
